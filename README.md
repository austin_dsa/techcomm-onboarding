# Techcomm Onboarding

## How do I join the organization?

Steps:
1. [Sign up for a gitlab account](https://gitlab.com/users/sign_up), if you don't already have one. Please note that all repos in Austin DSA's techcomm are public so you should only sign up to techcomm with an identity you are willing to make public. You can use an alias/handle if you like but please make sure this won't impede your partcipation or ability to respond in a timely manner to discussions etc
2. [Log an "intro ticket" on this gitlab repo](https://gitlab.com/dsausa/chapters/austin/techcomm-onboarding/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=). This ticket will help introduce you to the rest of techcomm and also provide documentation of skills and interests that will help us coordinate. The intro ticket will ping the techcomm co-chairs and they will then be able to vet you for full inclusion into the gitlab organization. 
3. [Check out #techcomm Austin DSA slack channel](https://app.slack.com/client/T4EF8RLHZ/C73TB75PX/) and introduce yourself.
4.  [Join the National Tech Committee organization](https://airtable.com/shrsadbAsPqiyfXGp).(optional though recommended) The advantage of doing this is it will give you access to national level tech committee organizing and will reduce friction in inter-chapter collaboration. 



## Comm Goals

#### ⚒️ serve the common good of the organization. 

First and foremost tech committee is a committee which serves others. While it is impossible to operate in any space void of political or policy convictions, techcomm's primary goal is to focus on the brass tacks "building crap" part of things. Our infrastructure must seek to support the chapter's needs rather than impose our ideas of how the organization should run. 

#### 💡 foster the skills and knowledge sharing of chapter members. 

Socialism is more than just voting for socialists, or attending protests. It's also about mutual aid. Each member of this organization has something to both learn and teach. We are hoping that participation in this committee will give our members energy rather than drain them of energy. We want to empower and encourage people, help them unwrap the black box of technology and to become capable owners of their means of production. 

#### 🐝 collaborate with other chapters nationally.

Every DSA chapter reserves a certain amount of independence and this is a good thing (especially as the needs of chapters differs based on their members and context), but we also want to cross pollinate!

#### 🌎 contribute to the worldwide open source community 

Open source is not purely socialistic but it is greatly aligned with our goals. In building our own projects we should seek to share our knowledge and skills with the rest of the open source community. This is itself a form of activism which can help change people's worldviews.

#### 📇 join forces with like minded organizations. 

There are many organizations out there with similarly aligned interests. Besides the open source community there are civic organizations, non-profits, artist collectives, and co-ops. By opening our projects to the public, we make them available for organizations that otherwise might not have the technical manpower to accomplish good goals. 

## FAQ

### What is Techcomm? 

Techcomm is the committee that helps Austin DSA run and build it's digital and electronic infrastructure. 

### Who is in charge?

You are! This is a member led committee. But if you want to know who the current co-chair organizers are, that would be [Aslan French](mailto:aslan@jackalope.tech) and Glenn W.

We're just here to facilitate member collaboration.


### How do I know what projects are active and need help?

You can see all the chapter projects [here](https://gitlab.com/dsausa/chapters/austin?sort=updated_desc) sorted by last updated. Any projects which are considered archived, or no longer maintained will have their repo [archived here](https://gitlab.com/groups/dsausa/chapters/austin/-/archived). Archived projects are read-only. If you want to work on a project that has been archived please post an issue [here](https://gitlab.com/dsausa/chapters/austin/techcomm-onboarding/issues)(there will be a default template, simply delete the contents of the template and fill out a normal ticket like you would on any other open source project) 


# Contact Info:

[techcomm co-chair: Aslan French](mailto:howdy@aslanfrench.work) 

[#techcomm Austin DSA slack channel](https://app.slack.com/client/T4EF8RLHZ/C73TB75PX/) 

[techcomm gmail: austindsatech@gmail.com](mailto:austindsatech@gmail.com)

[Join National Tech Committee](https://airtable.com/shrsadbAsPqiyfXGp)



# Code of conduct

This applies to all ATXDSA Repositories in addition to their own guides:

Follow basic expectations of decency and respect in any DSA space, as well as the [contributor covenant](https://www.contributor-covenant.org/). If you have an issue with someone and would like to address it with a 3rd party you can [check out this page](https://austindsa.org/contact) for contact information and resources from our Harassment Grievances Officers. 

